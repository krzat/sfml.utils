﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using SFML.Graphics;
using SFML.Window;
using SFML.System;

namespace SFML.Utils
{
    public abstract class Game
    {
        public View Camera { get; private set; }
        public RenderWindow Window { get; private set; }

        public Color ClearColor = Color.Black;

        public void Run(GameSettings settings = null)
        {
            if(settings == null) settings = new GameSettings();
            Window = settings.Create();
            Window.Closed += (s, a) => Window.Close();
            Window.Resized += Window_Resized;

            Camera = new View(Window.DefaultView);

            var watch = Stopwatch.StartNew();

            Initialize();
            while (Window.IsOpen)
            {
                var time = (float)watch.Elapsed.TotalSeconds;
                watch.Restart();

                Update(time);

                Window.Clear(ClearColor);
                Window.SetView(Camera);
                Draw(time);

                Window.Display();
                Window.DispatchEvents();
            }

        }

        void Window_Resized(object sender, SizeEventArgs e)
        {
            var size = new Vector2f(e.Width, e.Height);
            var cam = Camera.Size;

            var rX = cam.X/size.X;
            var rY = cam.Y/size.Y;

            cam = Math.Max(rX, rY) * size;

            Camera.Size = cam;
            Window.DefaultView.Size = size;
            Window.DefaultView.Center = size / 2;
        }

        protected abstract void Initialize();
        protected abstract void Draw(float dT);

        protected virtual void Update(float dT)
        {
        }
    }
}
