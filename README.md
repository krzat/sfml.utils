SFML.Utils
==========

Useful classes for SFML.Net library

* Vector extensions: conversion, dot, projection, comparision, rotation, angle, length, direction(normalization)
* Rect extensions: Bottom, Right, Position, Center, AABB from vectors, depth of intersection
* Spatial partitioning: quad tree, grid, hash
* Game class with basic game loop
* TileMap renderer
* SpriteBatch
* Texture update with Color array
* Misc: measure execution time of method, assert(throws exception), conditional break
* Fast sin and cos
* Content manager
