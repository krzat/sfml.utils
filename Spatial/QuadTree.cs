﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using SFML.Graphics;
using SFML.Window;
using SFML.System;

namespace SFML.Utils.Spatial
{
    //Not my work
    public class QuadTree<T> : ISpatialMap<T> where T : class, ISpatial
    {
        private readonly bool sort;
        private readonly Vector2f minLeafSize;
        private readonly int maxObjectsPerLeaf;
        private QuadNode root = null;
        private Dictionary<T, QuadNode> objectToNodeLookup = new Dictionary<T, QuadNode>();
        private Dictionary<T, int> objectSortOrder = new Dictionary<T, int>();
        public QuadNode Root { get { return root; } }
        private object syncLock = new object();
        private int objectSortId = 0;

        public QuadTree(Vector2f minLeafSize, int maxObjectsPerLeaf)
        {
            this.minLeafSize = minLeafSize;
            this.maxObjectsPerLeaf = maxObjectsPerLeaf;
        }

        public int GetSortOrder(T quadObject)
        {
            lock (objectSortOrder)
            {
                if (!objectSortOrder.ContainsKey(quadObject))
                    return -1;
                else
                {
                    return objectSortOrder[quadObject];
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="minLeafSize">The smallest Vector2f a leaf will split into</param>
        /// <param name="maxObjectsPerLeaf">Maximum number of objects per leaf before it forces a split into sub quadrants</param>
        /// <param name="sort">Whether or not queries will return objects in the order in which they were added</param>
        public QuadTree(Vector2f minLeafSize, int maxObjectsPerLeaf, bool sort)
            : this(minLeafSize, maxObjectsPerLeaf)
        {
            this.sort = sort;
        }

        FloatRect GetRect(Vector2f p, Vector2f size)
        {
            return new FloatRect(p.X, p.Y, size.X, size.Y);
        }

        public void Insert(T quadObject)
        {
            lock (syncLock)
            {
                if (sort & !objectSortOrder.ContainsKey(quadObject))
                {
                    objectSortOrder.Add(quadObject, objectSortId++);
                }

                FloatRect bounds = quadObject.AABB;
                if (root == null)
                {
                    var rootSize = new Vector2f((float)Math.Ceiling(bounds.Width / minLeafSize.X),
                                            (float)Math.Ceiling(bounds.Left / minLeafSize.Y));
                    var multiplier = Math.Max(rootSize.X, rootSize.Y);
                    rootSize = new Vector2f(minLeafSize.X * multiplier, minLeafSize.Y * multiplier);
                    var center = new Vector2f(bounds.Left + bounds.Width / 2, bounds.Top + bounds.Height / 2);
                    var rootOrigin = new Vector2f(center.X - rootSize.X / 2, center.Y - rootSize.Y / 2);
                    root = new QuadNode(GetRect(rootOrigin, rootSize));
                }
                while (!root.Bounds.Contains(bounds))
                {
                    ExpandRoot(bounds);
                }

                InsertNodeObject(root, quadObject);
            }
        }



        public List<T> Query(FloatRect bounds)
        {
            lock (syncLock)
            {
                List<T> results = new List<T>();
                if (root != null)
                    Query(bounds, root, results);
                if (sort)
                    results.Sort((a, b) => { return objectSortOrder[a].CompareTo(objectSortOrder[b]); });
                return results;
            }
        }

        private void Query(FloatRect bounds, QuadNode node, List<T> results)
        {
            lock (syncLock)
            {
                if (node == null) return;

                if (bounds.Intersects(node.Bounds))
                {
                    foreach (T quadObject in node.Objects)
                    {
                        if (bounds.Intersects(quadObject.AABB))
                            results.Add(quadObject);
                    }

                    foreach (QuadNode childNode in node.Nodes)
                    {
                        Query(bounds, childNode, results);
                    }
                }
            }
        }

        private void ExpandRoot(FloatRect newChildBounds)
        {
            lock (syncLock)
            {
                bool isNorth = root.Bounds.Top < newChildBounds.Top;
                bool isWest = root.Bounds.Left < newChildBounds.Left;

                DiFloatRection rootDiFloatRection;
                if (isNorth)
                {
                    rootDiFloatRection = isWest ? DiFloatRection.NW : DiFloatRection.NE;
                }
                else
                {
                    rootDiFloatRection = isWest ? DiFloatRection.SW : DiFloatRection.SE;
                }

                float newX = (rootDiFloatRection == DiFloatRection.NW || rootDiFloatRection == DiFloatRection.SW)
                                  ? root.Bounds.Left
                                  : root.Bounds.Left - root.Bounds.Width;
                float newY = (rootDiFloatRection == DiFloatRection.NW || rootDiFloatRection == DiFloatRection.NE)
                                  ? root.Bounds.Top
                                  : root.Bounds.Top - root.Bounds.Height;
                FloatRect newRootBounds = new FloatRect(newX, newY, root.Bounds.Width * 2, root.Bounds.Height * 2);
                QuadNode newRoot = new QuadNode(newRootBounds);
                SetupChildNodes(newRoot);
                newRoot[rootDiFloatRection] = root;
                root = newRoot;
            }
        }

        private void InsertNodeObject(QuadNode node, T quadObject)
        {
            lock (syncLock)
            {
                if (!node.Bounds.Contains(quadObject.AABB))
                    throw new Exception("This should not happen, child does not fit within node bounds");

                if (!node.HasChildNodes() && node.Objects.Count + 1 > maxObjectsPerLeaf)
                {
                    SetupChildNodes(node);

                    List<T> childObjects = new List<T>(node.Objects);
                    List<T> childrenToRelocate = new List<T>();

                    foreach (T childObject in childObjects)
                    {
                        foreach (QuadNode childNode in node.Nodes)
                        {
                            if (childNode == null)
                                continue;

                            if (childNode.Bounds.Contains(childObject.AABB))
                            {
                                childrenToRelocate.Add(childObject);
                            }
                        }
                    }

                    foreach (T childObject in childrenToRelocate)
                    {
                        RemoveQuadObjectFromNode(childObject);
                        InsertNodeObject(node, childObject);
                    }
                }

                foreach (QuadNode childNode in node.Nodes)
                {
                    if (childNode != null)
                    {
                        if (childNode.Bounds.Contains(quadObject.AABB))
                        {
                            InsertNodeObject(childNode, quadObject);
                            return;
                        }
                    }
                }

                AddQuadObjectToNode(node, quadObject);
            }
        }

        private void ClearQuadObjectsFromNode(QuadNode node)
        {
            lock (syncLock)
            {
                List<T> quadObjects = new List<T>(node.Objects);
                foreach (T quadObject in quadObjects)
                {
                    RemoveQuadObjectFromNode(quadObject);
                }
            }
        }

        private void RemoveQuadObjectFromNode(T quadObject)
        {
            lock (syncLock)
            {
                QuadNode node = objectToNodeLookup[quadObject];
                node.quadObjects.Remove(quadObject);
                objectToNodeLookup.Remove(quadObject);
                quadObject.AABBChanged -= quadObject_BoundsChanged;
            }
        }

        private void AddQuadObjectToNode(QuadNode node, T quadObject)
        {
            lock (syncLock)
            {
                node.quadObjects.Add(quadObject);
                objectToNodeLookup.Add(quadObject, node);
                quadObject.AABBChanged += quadObject_BoundsChanged;
            }
        }

        void quadObject_BoundsChanged(ISpatial sender)
        {
            lock (syncLock)
            {
                T quadObject = sender as T;
                if (quadObject != null)
                {
                    QuadNode node = objectToNodeLookup[quadObject];
                    if (!node.Bounds.Contains(quadObject.AABB) || node.HasChildNodes())
                    {
                        RemoveQuadObjectFromNode(quadObject);
                        Insert(quadObject);
                        if (node.Parent != null)
                        {
                            CheckChildNodes(node.Parent);
                        }
                    }
                }
            }
        }

        private void SetupChildNodes(QuadNode node)
        {
            lock (syncLock)
            {
                if (minLeafSize.X <= node.Bounds.Width / 2 && minLeafSize.Y <= node.Bounds.Height / 2)
                {
                    node[DiFloatRection.NW] = new QuadNode(node.Bounds.Left, node.Bounds.Top, node.Bounds.Width / 2,
                                                      node.Bounds.Height / 2);
                    node[DiFloatRection.NE] = new QuadNode(node.Bounds.Left + node.Bounds.Width / 2, node.Bounds.Top,
                                                      node.Bounds.Width / 2,
                                                      node.Bounds.Height / 2);
                    node[DiFloatRection.SW] = new QuadNode(node.Bounds.Left, node.Bounds.Top + node.Bounds.Height / 2,
                                                      node.Bounds.Width / 2,
                                                      node.Bounds.Height / 2);
                    node[DiFloatRection.SE] = new QuadNode(node.Bounds.Left + node.Bounds.Width / 2,
                                                      node.Bounds.Top + node.Bounds.Height / 2,
                                                      node.Bounds.Width / 2, node.Bounds.Height / 2);

                }
            }
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        bool ICollection<T>.Remove(T item)
        {
            throw new NotImplementedException();
        }

        public int Count { get; private set; }
        public bool IsReadOnly { get; private set; }

        public void Remove(T quadObject)
        {
            lock (syncLock)
            {
                if (sort && objectSortOrder.ContainsKey(quadObject))
                {
                    objectSortOrder.Remove(quadObject);
                }

                if (!objectToNodeLookup.ContainsKey(quadObject))
                    throw new KeyNotFoundException("QuadObject not found in dictionary for removal");

                QuadNode containingNode = objectToNodeLookup[quadObject];
                RemoveQuadObjectFromNode(quadObject);

                if (containingNode.Parent != null)
                    CheckChildNodes(containingNode.Parent);
            }
        }



        private void CheckChildNodes(QuadNode node)
        {
            lock (syncLock)
            {
                if (GetQuadObjectCount(node) <= maxObjectsPerLeaf)
                {
                    // Move child objects into this node, and delete sub nodes
                    List<T> subChildObjects = GetChildObjects(node);
                    foreach (T childObject in subChildObjects)
                    {
                        if (!node.Objects.Contains(childObject))
                        {
                            RemoveQuadObjectFromNode(childObject);
                            AddQuadObjectToNode(node, childObject);
                        }
                    }
                    if (node[DiFloatRection.NW] != null)
                    {
                        node[DiFloatRection.NW].Parent = null;
                        node[DiFloatRection.NW] = null;
                    }
                    if (node[DiFloatRection.NE] != null)
                    {
                        node[DiFloatRection.NE].Parent = null;
                        node[DiFloatRection.NE] = null;
                    }
                    if (node[DiFloatRection.SW] != null)
                    {
                        node[DiFloatRection.SW].Parent = null;
                        node[DiFloatRection.SW] = null;
                    }
                    if (node[DiFloatRection.SE] != null)
                    {
                        node[DiFloatRection.SE].Parent = null;
                        node[DiFloatRection.SE] = null;
                    }

                    if (node.Parent != null)
                        CheckChildNodes(node.Parent);
                    else
                    {
                        // Its the root node, see if we're down to one quadrant, with none in local storage - if so, ditch the other three
                        int numQuadrantsWithObjects = 0;
                        QuadNode nodeWithObjects = null;
                        foreach (QuadNode childNode in node.Nodes)
                        {
                            if (childNode != null && GetQuadObjectCount(childNode) > 0)
                            {
                                numQuadrantsWithObjects++;
                                nodeWithObjects = childNode;
                                if (numQuadrantsWithObjects > 1) break;
                            }
                        }
                        if (numQuadrantsWithObjects == 1)
                        {
                            foreach (QuadNode childNode in node.Nodes)
                            {
                                if (childNode != nodeWithObjects)
                                    childNode.Parent = null;
                            }
                            root = nodeWithObjects;
                        }
                    }
                }
            }
        }


        private List<T> GetChildObjects(QuadNode node)
        {
            lock (syncLock)
            {
                List<T> results = new List<T>();
                results.AddRange(node.quadObjects);
                foreach (QuadNode childNode in node.Nodes)
                {
                    if (childNode != null)
                        results.AddRange(GetChildObjects(childNode));
                }
                return results;
            }
        }

        public int GetQuadObjectCount()
        {
            lock (syncLock)
            {
                if (root == null)
                    return 0;
                int count = GetQuadObjectCount(root);
                return count;
            }
        }

        private int GetQuadObjectCount(QuadNode node)
        {
            lock (syncLock)
            {
                int count = node.Objects.Count;
                foreach (QuadNode childNode in node.Nodes)
                {
                    if (childNode != null)
                    {
                        count += GetQuadObjectCount(childNode);
                    }
                }
                return count;
            }
        }

        public int GetQuadNodeCount()
        {
            lock (syncLock)
            {
                if (root == null)
                    return 0;
                int count = GetQuadNodeCount(root, 1);
                return count;
            }
        }

        private int GetQuadNodeCount(QuadNode node, int count)
        {
            lock (syncLock)
            {
                if (node == null) return count;

                foreach (QuadNode childNode in node.Nodes)
                {
                    if (childNode != null)
                        count++;
                }
                return count;
            }
        }

        public List<QuadNode> GetAllNodes()
        {
            lock (syncLock)
            {
                List<QuadNode> results = new List<QuadNode>();
                if (root != null)
                {
                    results.Add(root);
                    GetChildNodes(root, results);
                }
                return results;
            }
        }

        private void GetChildNodes(QuadNode node, ICollection<QuadNode> results)
        {
            lock (syncLock)
            {
                foreach (QuadNode childNode in node.Nodes)
                {
                    if (childNode != null)
                    {
                        results.Add(childNode);
                        GetChildNodes(childNode, results);
                    }
                }
            }
        }

        public class QuadNode
        {
            private static int _id = 0;
            public readonly int ID = _id++;

            public QuadNode Parent { get; internal set; }

            private QuadNode[] _nodes = new QuadNode[4];
            public QuadNode this[DiFloatRection diFloatRection]
            {
                get
                {
                    switch (diFloatRection)
                    {
                        case DiFloatRection.NW:
                            return _nodes[0];
                        case DiFloatRection.NE:
                            return _nodes[1];
                        case DiFloatRection.SW:
                            return _nodes[2];
                        case DiFloatRection.SE:
                            return _nodes[3];
                        default:
                            return null;
                    }
                }
                set
                {
                    switch (diFloatRection)
                    {
                        case DiFloatRection.NW:
                            _nodes[0] = value;
                            break;
                        case DiFloatRection.NE:
                            _nodes[1] = value;
                            break;
                        case DiFloatRection.SW:
                            _nodes[2] = value;
                            break;
                        case DiFloatRection.SE:
                            _nodes[3] = value;
                            break;
                    }
                    if (value != null)
                        value.Parent = this;
                }
            }

            public ReadOnlyCollection<QuadNode> Nodes;

            internal List<T> quadObjects = new List<T>();
            public ReadOnlyCollection<T> Objects;

            public FloatRect Bounds { get; internal set; }

            public bool HasChildNodes()
            {
                return _nodes[0] != null;
            }

            public QuadNode(FloatRect bounds)
            {
                Bounds = bounds;
                Nodes = new ReadOnlyCollection<QuadNode>(_nodes);
                Objects = new ReadOnlyCollection<T>(quadObjects);
            }

            public QuadNode(float x, float y, float width, float height)
                : this(new FloatRect(x, y, width, height))
            {

            }
        }

        public IEnumerable<T> Select(FloatRect region)
        {
            return Query(region);
        }

        public IEnumerable<T> Select(Vector2f v)
        {
            //probably bad idea
            return Select(v.GetGlobalBounds(1));
        }

        public void Add(T item)
        {
            Insert(item);
        }

        public void Clear()
        {
            //throw new NotImplementedException();
        }

        public bool Contains(T item)
        {
            return objectToNodeLookup.ContainsKey(item);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return this.objectToNodeLookup.Keys.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    public enum DiFloatRection : int
    {
        NW = 0,
        NE = 1,
        SW = 2,
        SE = 3
    }
}