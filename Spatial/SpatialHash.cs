﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using SFML.Graphics;
using SFML.Window;
using System.Linq;
using SFML.System;

namespace SFML.Utils.Spatial
{
    public class SpatialHash<T> : ISpatialMap<T> where T:ISpatial
    {
        class Bucket : HashSet<T>
        {
            public Vector2i Key;

            public override string ToString()
            {
                return "Bucket " + Count + " " + Key;
            }
        }


        private Stack<Bucket> pool = new Stack<Bucket>(); 
        private Dictionary<Vector2i,Bucket> buckets = new Dictionary<Vector2i, Bucket>();
        private Dictionary<T, FloatRect> items = new Dictionary<T, FloatRect>();

        public readonly float Size = 20;

        public SpatialHash(float size)
        {
            Size = size;
        }

        public IEnumerable<T> Select(FloatRect region)
        {
            foreach (var bucket in GetBuckets(region, false))
            {
                foreach (var item in bucket)
                {
                    var aabb = items[item];
                    if (aabb.Intersects(region)) yield return item;
                }
            }
        }

        public IEnumerable<T> Select(Vector2f p)
        {
            Bucket bucket;
            var key = ToKey(p.X, p.Y);
            if (buckets.TryGetValue(key, out bucket))
            {
                return bucket;
            }
            return Enumerable.Empty<T>();
        }

        public void Add(T item)
        {
            item.AABBChanged += item_AABBChanged;
            var rec = item.AABB;
            items.Add(item, rec);

            foreach (var bucket in GetBuckets(rec, true))
            {
                bucket.Add(item);
            }
        }

        public void Clear()
        {
            var toRemove = items.Keys.ToArray();
            foreach (var obj in toRemove)
            {
                Remove(obj);
            }

            
            buckets.Clear();

        }

        public bool Contains(T item)
        {
            return items.Keys.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            items.Keys.CopyTo(array, arrayIndex);
        }

        public int Count { get; private set; }
        public bool IsReadOnly { get; private set; }

        void item_AABBChanged(ISpatial spatial)
        {
            var obj = (T) spatial;
            var aabb = obj.AABB;
            var oldBuckets = GetBuckets(items[obj], false).ToArray();
            items[obj] = aabb;
            var newBuckets = GetBuckets(aabb, true).ToArray();

            foreach (var bucket in newBuckets.Except(oldBuckets))
                bucket.Add(obj);

            foreach (var bucket in oldBuckets.Except(newBuckets))
            {
                bucket.Remove(obj);
                if(bucket.Count == 0) RemoveBucket(bucket);
            }

        }
        public bool Remove(T item)
        {
            var rec = items[item];
            item.AABBChanged -= item_AABBChanged;
            foreach (var bucket in GetBuckets(rec, false))
            {
                bucket.Remove(item);
                if (bucket.Count == 0)
                    RemoveBucket(bucket);
            }

            return true;
        }

        void RemoveBucket(Bucket bucket)
        {
            buckets.Remove(bucket.Key);
            if (pool.Count < 100)
                pool.Push(bucket);
        }

        Vector2i ToKey(float x, float y)
        {
            x /= Size;
            y /= Size;
            if (x < 0) x--;
            if (y < 0) y--;
            
            return new Vector2i((int)x, (int)y);
        }


        IEnumerable<Bucket> GetBuckets(FloatRect rec, bool createMissing)
        {
            var b = ToKey(rec.Left, rec.Top);
            var e = ToKey(rec.Right(), rec.Bottom());

            for (int x = b.X; x <= e.X; x++)
                for (int y = b.Y; y <= e.Y; y++)
                {
                    var key = new Vector2i(x, y);
                    Bucket bucket;
                    if (buckets.TryGetValue(key, out bucket))
                    {
                        yield return bucket;
                    }
                    else if (createMissing)
                    {
                        if (pool.Count > 0)
                            bucket = pool.Pop();

                        else bucket = new Bucket();
                        buckets.Add(key, bucket);
                        bucket.Key = key;
                        yield return bucket;
                    }
                }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return items.Keys.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}